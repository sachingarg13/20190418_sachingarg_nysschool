//
//  SchoolCell.swift
//  20190419-SachinGarg-NYCSchools
//
//  Created by Sachin Garg on 4/18/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {

    
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var schoolName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setSchoolCellWith(school: SchoolList) {
        
        DispatchQueue.main.async {
            self.schoolName.text = school.schoolname
            self.mathScore.text = school.dbn
            
        }
    }
    
}
