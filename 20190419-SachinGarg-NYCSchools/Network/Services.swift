//
//  Services.swift
//  20190419-SachinGarg-NYCSchools
//
//  Created by Sachin Garg on 4/18/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import Foundation
import UIKit

class Services: NSObject {
    
    lazy var endPoint: String = {
        return "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    }()
    
    func getDataWith(completion: @escaping (Result<[[String: AnyObject]]>) -> Void) {
        
        let urlString = endPoint
        
        guard let url = URL(string: urlString) else { return completion(.Error("Invalid URL, we can't update")) }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
            }
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers])
                guard let jsonArray = jsonResponse as? [[String: AnyObject]] else {
                    return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
                }
                // print(jsonArray)
                
                DispatchQueue.main.async {
                    completion(.Success(jsonArray))
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            
            }.resume()
    }
}

enum Result<T> {
    case Success(T)
    case Error(String)
}

