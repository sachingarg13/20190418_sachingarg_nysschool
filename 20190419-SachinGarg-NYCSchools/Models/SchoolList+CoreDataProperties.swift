//
//  SchoolList+CoreDataProperties.swift
//  20190419-SachinGarg-NYCSchools
//
//  Created by Sachin Garg on 4/18/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//
//

import Foundation
import CoreData


extension SchoolList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SchoolList> {
        return NSFetchRequest<SchoolList>(entityName: "SchoolList")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var mathScore: Int16
    @NSManaged public var readingScore: Int16
    @NSManaged public var schoolname: String?
    @NSManaged public var writingScore: Int16

}
