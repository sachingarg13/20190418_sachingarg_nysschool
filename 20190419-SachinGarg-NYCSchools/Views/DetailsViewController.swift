//
//  DetailsViewController.swift
//  20190419-SachinGarg-NYCSchools
//
//  Created by Sachin Garg on 4/18/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit
import CoreData

class DetailsViewController: UIViewController {
    @IBOutlet weak var schoolName: UILabel!
    
    @IBOutlet weak var lblRdngScore: UILabel!
    @IBOutlet weak var lblWritScore: UILabel!
    
    @IBAction func btnWebsite(_ sender: Any) {
       // UIApplication.shared.open(URL(string: urlSting)!, options: [:], completionHandler: nil)
            }
    @IBOutlet weak var lblMathScore: UILabel!
    var bdn:String?
    var urlSting:String = "http:\\www.google.com"


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "School Average Score"

        getData()
        // Do any additional setup after loading the view.
    }
    
    //Get school records data by bdn
    func getData()
    {
        let endPointString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(bdn ?? "01M292")"
        
        guard let url = URL(string: endPointString) else {
            print("error with url string")
            return
        }
        let urlrequest = URLRequest(url: url)
        let session = URLSession.shared
        session.dataTask(with: urlrequest) { (data, response, error) in
            guard error == nil else
            {
                print("Error in response")
                return
            }
            guard let respData = data else {
                print("No data in response")
                return
            }
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: respData, options: [])
                guard let jsonArray = jsonResponse as? [[String: Any]] else {
                    return
                }
                // print(jsonArray)
                if jsonArray.count == 0
                {
                    self.showAlertWith(title: "Sorry", message: "No Data available this time, try again later")
                }
                for dic in jsonArray{
                    guard let title = dic["school_name"] as? String else { return }
                    DispatchQueue.main.async {
                    self.schoolName.text = title
                    self.lblMathScore.text = dic["sat_math_avg_score"] as? String
                    self.lblRdngScore.text = dic["sat_critical_reading_avg_score"] as? String
                    self.lblWritScore.text = dic["sat_writing_avg_score"] as? String
                    }
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            }.resume()
        
    }
    func showAlertWith(title: String, message: String, style: UIAlertController.Style = .alert) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


