//
//  ViewController.swift
//  20190419-SachinGarg-NYCSchools
//
//  Created by Sachin Garg on 4/17/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController {
    
    @IBOutlet weak var tblvSchool: UITableView!
    var selectedIndexPath:IndexPath = []
    
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: SchoolList.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "schoolname", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataObjects.sharedInstance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "School List"
        tblvSchool.delegate = self
        tblvSchool.dataSource = self
        registerCells()
        updateTableContent()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func registerCells() {
        tblvSchool.register(UINib.init(nibName: "SchoolCell", bundle: nil), forCellReuseIdentifier: "SchoolCell")
    }
    
    func updateTableContent() {
        
        do {
            try self.fetchedhResultController.performFetch()
            print("COUNT FETCHED FIRST: \(String(describing: self.fetchedhResultController.sections?[0].numberOfObjects))")
        } catch let error  {
            print("ERROR: \(error)")
        }
        
        let service = Services()
        service.getDataWith { (result) in
            switch result {
            case .Success(let data):
                self.clearData() // Clear the existing data on success data retrival
                self.saveInCoreDataWith(array: data)
            case .Error(let message):
                DispatchQueue.main.async {
                    self.showAlertWith(title: "Error", message: message)
                }
            }
        }
    }
    
    func showAlertWith(title: String, message: String, style: UIAlertController.Style = .alert) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let action = UIAlertAction(title: title, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    private func clearData() {
        do {
            
            let context = CoreDataObjects.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: SchoolList.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                CoreDataObjects.sharedInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    
    
    private func saveInCoreDataWith(array: [[String: AnyObject]]) {
        _ = array.map{self.createSchoolEntityFrom(dictionary: $0)}
        do {
            try CoreDataObjects.sharedInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    private func createSchoolEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
        
        let context = CoreDataObjects.sharedInstance.persistentContainer.viewContext
        if let schoolEntity = NSEntityDescription.insertNewObject(forEntityName: "SchoolList", into: context) as? SchoolList {
            schoolEntity.schoolname = dictionary["school_name"] as? String
            schoolEntity.dbn = dictionary["dbn"] as? String
            
            return schoolEntity
        }
        return nil
    }

   
}



extension MainViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.tblvSchool.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.tblvSchool.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tblvSchool.endUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblvSchool.beginUpdates()
    }
}
extension MainViewController : UITableViewDelegate
{

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndexPath = indexPath as IndexPath
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "detailView") as! DetailsViewController
        if let school = fetchedhResultController.object(at: indexPath) as? SchoolList {
            vc.bdn = school.dbn
        }
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.beginUpdates()
        tableView.endUpdates()
        //tblvSchool.beginUpdates
        //tblvSchool.endUpdates
    }
    
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if selectedIndexPath == indexPath as IndexPath {
            return 60
        }
        return 60.0
    }
}
extension MainViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = fetchedhResultController.sections?.first?.numberOfObjects {
            return count
        }
        return 0
    }
    
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedIndexPath = indexPath as IndexPath
        tableView.beginUpdates()
        tableView.endUpdates()
   
    }

    


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SchoolCell = self.tblvSchool.dequeueReusableCell(withIdentifier: "SchoolCell") as! SchoolCell
        if let school = fetchedhResultController.object(at: indexPath) as? SchoolList {
            cell.setSchoolCellWith(school: school)
        }
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}


